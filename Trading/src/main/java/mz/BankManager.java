package mz;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

public class BankManager 
{
	private HashMap<String, Position> positionMap;
	private HashMap<Integer, TradeRecord> recordMap;

	public BankManager(){
		positionMap = new HashMap<String, Position>();
		recordMap = new HashMap<Integer, TradeRecord>();
	}

	public void process(String fileName){
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line;

			TradeRecord trade;
			while ((line = br.readLine()) != null) {
				if(line.trim().isEmpty()){
					continue;
				}

				trade = createRecord(line);
				updateManager(trade);
			}
			
			printPositions();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private void printPositions() {
		for(Position p : positionMap.values()){
			System.out.println(p);
		}		
	}

	private void updateManager(TradeRecord record) throws Exception {
		TradeRecord prevRecord = recordMap.get(record.getId());
		Position pos = positionMap.get(record.getPosKey());

		if(prevRecord == null){
			// first record for (trade_id)
			
			if(pos == null){
				// first record for (account, security_identifier)
				positionMap.put(record.getPosKey(), new Position(record));
			}else{
				pos.applyTrade(record);
			}

			recordMap.put(record.getId(), record);

		}else{

			if(prevRecord.getVersion()<record.getVersion()){
				/* this record is not the first record for (trade_id) 
				 * and previous record's version is lower,
				 * so reverse previous */
				
				Position prevPos = positionMap.get(prevRecord.getPosKey());
				prevPos.reverseTrade(prevRecord);

				if(pos == null){
					positionMap.put(record.getPosKey(), new Position(record));
				}else{
					pos.applyTrade(record);
				}
				
				recordMap.put(record.getId(), record);

			}else if(prevRecord.getVersion()>record.getVersion()){
				// higher version comes earlier then lower version does
				
				if(prevRecord.getPosKey().equals(record.getPosKey())){
					//do nothing  
				}else{
					if(pos == null){
						positionMap.put(record.getPosKey(), new Position(record.getId(), record.getAcc(), record.getIdentifier()));
					}
				}
			}else{
				throw new Exception("Same ID and same version( " + prevRecord +"  "+ record +" )");
			}		

		}

	}

	private TradeRecord createRecord(String record) {
		String[] strs = record.trim().split("\\|");

		int id = Integer.parseInt(strs[0]);
		int version = Integer.parseInt(strs[1]);
		String identifier = strs[2];
		int quantity = Integer.parseInt(strs[3]);
		String direction = strs[4];
		int acc = Integer.parseInt((strs[5].split("-"))[1]);
		String operation = strs[6];
		return new TradeRecord(id, version, identifier, quantity, direction, acc, operation);
	}

	public static void main( String[] args )
	{
		BankManager bm = new BankManager();
		bm.process(args[0]);
	}
}
