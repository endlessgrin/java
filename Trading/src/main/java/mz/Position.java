package mz;

import java.util.*;

public class Position {
	private final int acc;
	private final String identifier;
	private int quantity;
	private final HashSet<Integer> tradeSet;

	public Position(final TradeRecord record) {
		this.acc = record.getAcc();
		this.identifier = record.getIdentifier();
		this.quantity = record.computeAmount();		
		this.tradeSet = new HashSet<Integer>();
		this.tradeSet.add(record.getId());
	}

	public Position(int tid, int acc, String identifier) {
		this.acc = acc;
		this.identifier = identifier.toUpperCase();
		this.quantity = 0;
		this.tradeSet = new HashSet<Integer>();
		this.tradeSet.add(tid);
	}

	public void applyTrade(final TradeRecord record){
		//Manager has checked acc and identifier. should they be checked here too?
		this.tradeSet.add(record.getId());
		this.quantity += record.computeAmount();
	}

	public void reverseTrade(final TradeRecord record){
		this.quantity -= record.computeAmount();
	}

	@Override
	public String toString(){
		String s = "ACC-"+acc+"|"+identifier+"|"+quantity+"|";
		for(Integer id : tradeSet){
			s += id+",";
		}
		return s.substring(0, s.length()-1);
	}
}
