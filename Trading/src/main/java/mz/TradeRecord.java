package mz;

import java.util.*;


public class TradeRecord {

	public enum Direction{
		BUY, SELL
	}

	public enum Operation{
		NEW, AMEND, CANCEL
	}

	private final int id;
	private final int version;
	private final String identifier;
	private final int quantity;
	private final Direction dir;
	private final int acc;
	private final Operation op;

	public TradeRecord(int id, int version, String identifier, int quantity, String direction, int acc, String operation){
		this.id = id;
		this.version = version;
		this.identifier = identifier.toUpperCase();
		
		if(quantity<0){
			throw new IllegalArgumentException("negative quantity: " + quantity);
		}
		this.quantity = quantity;

		switch(direction.toUpperCase()){
		case "BUY":
			this.dir = Direction.BUY;
			break;
		case "SELL":
			this.dir = Direction.SELL;
			break;
		default:
			throw new IllegalArgumentException("Invalid direction: " + direction);
		}

		this.acc = acc;

		switch(operation.toUpperCase()){
		case "NEW":
			this.op = Operation.NEW;
			break;
		case "AMEND":
			this.op = Operation.AMEND;
			break;
		case "CANCEL":
			this.op = Operation.CANCEL;
			break;
		default:
			throw new IllegalArgumentException("Invalid operation: " + operation);
		}		
	}

	public int getId(){
		return id;
	}

	public String getPosKey() {		
		return acc + identifier;
	}

	public int getVersion() {		
		return version;
	}

	public String getIdentifier() {
		return identifier;
	}

	public int getAcc() {
		return acc;
	}

	@Override
	public String toString(){
		return "id: "+id + "  version:"+ version;
	}

	public int computeAmount() {
		int amount = quantity;

		if((this.dir == Direction.BUY && this.op == Operation.CANCEL)
				|| (this.dir==Direction.SELL && (this.op != Operation.CANCEL))){
			amount = -amount;
		}

		return amount;
	}	

}
