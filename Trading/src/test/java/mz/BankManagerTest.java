package mz;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

public class BankManagerTest 
{
	@Test
	public void TestUpdateManager(){
		/*TODO: test cases
		 * input step:
		 *  a. 1234|1|XYZ|100|BUY|ACC-1234|NEW

			b. 5678|1|XYZ|200|BUY|ACC-1234|NEW
	 
			c. 1234|4|XYZ|150|SELL|ACC-1234|AMEND
	
			d. 1234|3|XYZ|150|SELL|ACC-4511|AMEND

			e. 1234|2|XYZ|100|SELL|ACC-1234|AMEND
	 
	
	 		corresponding output positions per step:
			a. ACC-1234|XYZ|100|1234

			b. ACC-1234|XYZ|300|1234, 5678

			c. ACC-1234|XYZ|50|1234, 5678

			d. ACC-1234|XYZ|50|1234, 5678   ACC-4511|XYZ|0|1234

			e. ACC-1234|XYZ|50|1234, 5678   ACC-4511|XYZ|0|1234
		 */
		
		BankManager bm = new BankManager();
		
		Method mUpdateManager;
		Field fPositionMap;
		Field fQuantity;
		Field fTradeSet;
		
		HashMap<String, Position> positionMap;
		HashMap<String, Position> expectedPositionMap;
		
		try {
			mUpdateManager = BankManager.class.getDeclaredMethod("updateManager", TradeRecord.class);
			mUpdateManager.setAccessible(true);
			
			fPositionMap = BankManager.class.getDeclaredField("positionMap");
			fPositionMap.setAccessible(true);
			positionMap = (HashMap<String, Position>) fPositionMap.get(bm);
			expectedPositionMap = new HashMap<String, Position>();	
			
			fQuantity = Position.class.getDeclaredField("quantity");
			fQuantity.setAccessible(true);
			fTradeSet = Position.class.getDeclaredField("tradeSet");
			fTradeSet.setAccessible(true);
			
			Assert.assertEquals(expectedPositionMap.size(), positionMap.size());
						
			int id=1234;
			int version=1;
			String identifier ="XYZ";
			int quantity = 100; 
			String direction = "BUY";
			int acc = 1234;
			String operation = "NEW";
			Position ePos = new Position(id, acc, identifier);
			fQuantity.set(ePos, 100);
			expectedPositionMap.put(acc+identifier, ePos);
			verifyPositionMap(expectedPositionMap, positionMap, mUpdateManager, bm, id, version, identifier, quantity, direction, acc, operation);
			
			id = 5678;
			version = 1;
			identifier ="XYZ";
			quantity = 200;
			direction ="BUY";
			acc = 1234;
			operation = "NEW";
			fQuantity.set(ePos, 300);
			HashSet<Integer> tradeSet = (HashSet<Integer>) fTradeSet.get(ePos);
			tradeSet.add(id);
			verifyPositionMap(expectedPositionMap, positionMap, mUpdateManager, bm, id, version, identifier, quantity, direction, acc, operation);
			
			id = 1234;
			version = 4;
			identifier ="XYZ";
			quantity = 150;
			direction ="SELL";
			acc = 1234;
			operation = "AMEND";
			fQuantity.set(ePos, 50);
			verifyPositionMap(expectedPositionMap, positionMap, mUpdateManager, bm, id, version, identifier, quantity, direction, acc, operation);
			
			id = 1234;
			version = 3;
			identifier ="XYZ";
			quantity = 150;
			direction ="SELL";
			acc = 4511;
			operation = "AMEND";
			Position ePos2 = new Position(id, acc, identifier);
			fQuantity.set(ePos2, 0);
			expectedPositionMap.put(acc+identifier, ePos2);
			verifyPositionMap(expectedPositionMap, positionMap, mUpdateManager, bm, id, version, identifier, quantity, direction, acc, operation);
			
			id = 1234;
			version = 2;
			identifier ="XYZ";
			quantity = 100;
			direction ="SELL";
			acc = 1234;
			operation = "AMEND";
			verifyPositionMap(expectedPositionMap, positionMap, mUpdateManager, bm, id, version, identifier, quantity, direction, acc, operation);
			
//			Position curPos = new Position(tr);
//			expectedPositionMap.put(tr.getPosKey(), curPos);
			
						
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void verifyPositionMap(HashMap<String, Position> expectedPositionMap, HashMap<String, Position> positionMap, Method mUpdateManager, BankManager bm,
			int id, int version, String identifier, int quantity, String direction, int acc, String operation) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		computePositionMap(mUpdateManager, bm, id, version, identifier, quantity, direction, acc, operation);
		assertEquality(expectedPositionMap , positionMap);
	}

	private void computePositionMap(Method mUpdateManager, BankManager bm, int id, int version, String identifier,
			int quantity, String direction, int acc, String operation) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		TradeRecord tr = new TradeRecord(id, version, identifier, quantity, direction, acc, operation);
		mUpdateManager.invoke(bm, tr);		
	}
	
	private void assertEquality(HashMap<String, Position> expectedPositionMap, HashMap<String, Position> positionMap) {
		Assert.assertEquals(expectedPositionMap.size(), positionMap.size());
		
		for(String posKey: expectedPositionMap.keySet()){
			verifyPosition(expectedPositionMap.get(posKey), positionMap.get(posKey));
		}
	}

	private void verifyPosition(Position expectedPosition, Position position){
		Assert.assertFalse(expectedPosition == null);
		Assert.assertFalse(position == null);
		verifyAcc(expectedPosition, position);
		verifyIdentifier(expectedPosition, position);
		verifyQuantity(expectedPosition, position);
		verifyTradeSet(expectedPosition, position);
	}
	
	private void verifyAcc(final Position expectedPosition, final Position position){
		Field fAcc;
		try {
			fAcc = Position.class.getDeclaredField("acc");
			fAcc.setAccessible(true);
			
			Assert.assertEquals(fAcc.get(expectedPosition), fAcc.get(position));
			
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void verifyIdentifier(final Position expectedPosition, final Position position){
		Field fIdentifier;
		try {
			fIdentifier = Position.class.getDeclaredField("identifier");
			fIdentifier.setAccessible(true);
			
			Assert.assertEquals(fIdentifier.get(expectedPosition), fIdentifier.get(position));
			
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void verifyQuantity(final Position expectedPosition, final Position position){
		Field fQuantity;
		try {
			fQuantity = Position.class.getDeclaredField("quantity");
			fQuantity.setAccessible(true);
			
			Assert.assertEquals(fQuantity.get(expectedPosition), fQuantity.get(position));
			
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void verifyTradeSet(final Position expectedPosition, final Position position){
		Field fTradeSet;
		try {
			fTradeSet = Position.class.getDeclaredField("tradeSet");
			fTradeSet.setAccessible(true);
			HashSet<Integer> expectedTradeSet = (HashSet<Integer>) fTradeSet.get(expectedPosition);
			HashSet<Integer> tradeSet = (HashSet<Integer>) fTradeSet.get(position);
			
			Assert.assertEquals(expectedTradeSet.size(), tradeSet.size());
			for(Integer id: tradeSet){
				Assert.assertEquals(true, expectedTradeSet.contains(id));
			}
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
