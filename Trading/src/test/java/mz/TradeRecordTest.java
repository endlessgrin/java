package mz;

import org.junit.Assert;
import org.junit.Test;

public class TradeRecordTest {
	@Test
	public void testComputeAmount(){
		int id=101;
		int version=2;
		String identifier ="xyz";
		int quantity = 500; 
		String direction = "buy";
		int acc = 9998;
		String operation = "new";
		
		TradeRecord tr = new TradeRecord(id, version, identifier, quantity, direction, acc, operation);
		Assert.assertEquals(quantity, tr.computeAmount());
		
		operation = "amend";
		tr = new TradeRecord(id, version, identifier, quantity, direction, acc, operation); 
		Assert.assertEquals(quantity, tr.computeAmount());
		
		operation = "cancel";
		tr = new TradeRecord(id, version, identifier, quantity, direction, acc, operation); 
		Assert.assertEquals(-1 * quantity, tr.computeAmount());
		
		direction = "sell";
		operation = "new";
		tr = new TradeRecord(id, version, identifier, quantity, direction, acc, operation); 
		Assert.assertEquals(-1 * quantity, tr.computeAmount());
		
		operation = "amend";
		tr = new TradeRecord(id, version, identifier, quantity, direction, acc, operation); 
		Assert.assertEquals(-1 * quantity, tr.computeAmount());
		
		operation = "cancel";
		tr = new TradeRecord(id, version, identifier, quantity, direction, acc, operation); 
		Assert.assertEquals(quantity, tr.computeAmount());
	}
	
	@Test
	public void testValidConstructor(){
		//TODO: test valid parameters of operation, direction, quantity, etc.
	}
}
