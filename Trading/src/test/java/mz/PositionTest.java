package mz;

import org.junit.Assert;
import org.junit.Test;
import java.lang.reflect.Field;
import java.util.HashSet;
public class PositionTest {
	@Test
	public void testApplyTrade(){
		int id=101;
		int version=2;
		String identifier ="xyz";
		int quantity = 500; 
		String direction = "buy";
		int acc = 9998;
		String operation = "new";
		try {
			TradeRecord tr = new TradeRecord(id, version, identifier, quantity, direction, acc, operation);

			Position p = new Position(tr);
			Field fTradeSet = Position.class.getDeclaredField("tradeSet");
			fTradeSet.setAccessible(true);
			HashSet<Integer> tradeSet = (HashSet<Integer>) fTradeSet.get(p);
			HashSet<Integer> expectedTradesSet = new HashSet<Integer>();
			expectedTradesSet.add(id);
			Assert.assertArrayEquals(expectedTradesSet.toArray(), tradeSet.toArray());
			Field fQuantity = Position.class.getDeclaredField("quantity");
			fQuantity.setAccessible(true);
			Assert.assertEquals(quantity, fQuantity.get(p));			
		
			int id2 = id+1;
			int quantity2 = 500;
			tr = new TradeRecord(id2, version, identifier, quantity2, direction, acc, operation);
			p.applyTrade(tr);
			expectedTradesSet.add(id2);
			Assert.assertArrayEquals(expectedTradesSet.toArray(), tradeSet.toArray());
			
			Assert.assertEquals(quantity+quantity2, fQuantity.get(p));
			/*more cases: 
				operation: cancel, amend
				direction: sell*/
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testReverseTrade(){

	}

	@Test
	public void testConstructor(){

	}
}
